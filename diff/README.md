# @dubsar/diff

An opinionated embedding in
[@dubsar/result](https://www.npmjs.com/package/@dubsar/result) of the amazing
library:

- [deep-object-diff](https://www.npmjs.com/package/deep-object-diff)

## api

```typescript
import { Result } from "@dubsar/result";

interface DetailedDiff<T> {
  readonly added: Partial<T>;
  readonly updated: Partial<T>;
  readonly deleted: Partial<T>;
}
declare const diff: <T extends object>(
  original: T,
  updated: Partial<T>,
) => DetailedDiff<T>;
interface DiffRecord<T, K extends keyof T = keyof T> {
  key: K;
  value: T[K];
}
declare class Diff<T extends object> {
  private original;
  private readonly added;
  private readonly updated;
  private readonly deleted;
  private constructor();
  static readonly build: <T_1 extends object>(
    original: T_1,
    updated: Partial<T_1>,
  ) => Diff<T_1>;
  readonly hasAdditions: () => boolean;
  readonly hasUpdates: () => boolean;
  readonly hasDeletetions: () => boolean;
  readonly areTheSame: () => boolean;
  private readonly getAddition;
  private readonly getUpdate;
  private readonly getDeletion;
  get additions(): Result<DiffRecord<T, keyof T>[]>;
  get updates(): Result<DiffRecord<T, keyof T>[]>;
  get deletions(): Result<DiffRecord<T, keyof T>[]>;
  readonly update: (t?: T) => Result<T>;
  readonly diff: () => DetailedDiff<T>;
}

export { DetailedDiff, Diff, diff };
```

## licence

[The MIT Licence (Expat).](LICENCE.md)
