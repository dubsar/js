import { detailedDiff } from "deep-object-diff";
import { Id, Result } from "@dubsar/result";

const defined = <T>(t: T | null | undefined): Result<T> => {
  const ok = !(t === undefined || t === null);
  if (ok) {
    return Result.ok(t);
  }
  const type = typeof t === "undefined" ? "undefined" : "null";
  return Result.error(`Result is ${type}`);
};

export interface DetailedDiff<T> {
  readonly added: Partial<T>;
  readonly updated: Partial<T>;
  readonly deleted: Partial<T>;
}

export const diff = <T extends object>(
  original: T,
  updated: Partial<T>,
): DetailedDiff<T> => {
  return detailedDiff(original, updated) as DetailedDiff<T>;
};

interface DiffRecord<T, K extends keyof T = keyof T> {
  key: K;
  value: T[K];
}

const record = <T, K extends keyof T = keyof T>(
  key: K,
  value?: T[K],
): Result<DiffRecord<T>> =>
  defined(value).match(
    (value) => Result.ok({ key, value: value! }),
    (_) => Result.error(`${key as string} not found`),
  );

export class Diff<T extends object> {
  private original: T;
  private readonly added: Partial<T>;
  private readonly updated: Partial<T>;
  private readonly deleted: Partial<T>;
  private constructor(original: T, updated_: Partial<T>) {
    const { added, updated, deleted } = diff(original, updated_);
    this.original = original;
    this.added = added;
    this.updated = updated;
    this.deleted = deleted;
  }

  static readonly build = <T extends object>(
    original: T,
    updated: Partial<T>,
  ): Diff<T> => new Diff(original, updated);

  readonly hasAdditions = (): boolean => Object.entries(this.added).length > 0;
  readonly hasUpdates = (): boolean => Object.entries(this.updated).length > 0;
  readonly hasDeletetions = (): boolean =>
    Object.entries(this.deleted).length > 0;

  readonly areTheSame = (): boolean =>
    ![this.hasAdditions(), this.hasUpdates(), this.hasDeletetions()]
      .reduce((acc, s) => acc || s, false);

  private readonly getAddition = <K extends keyof T>(p: K) =>
    record<T>(p, this.added[p]);
  private readonly getUpdate = <K extends keyof T>(p: K) =>
    record<T>(p, this.updated[p]);
  private readonly getDeletion = <K extends keyof T>(p: K) =>
    record<T>(p, this.deleted[p]);

  get additions() {
    return Result.reduce(
      (Object.keys(this.added) as [keyof T]).map(this.getAddition),
      Id,
    );
  }
  get updates() {
    return Result.reduce(
      (Object.keys(this.updated) as [keyof T]).map(this.getUpdate),
      Id,
    );
  }
  get deletions() {
    return Result.reduce(
      (Object.keys(this.deleted) as [keyof T]).map(this.getDeletion),
      Id,
    );
  }

  readonly update = (t: T = this.original): Result<T> =>
    this.updates.match(
      (diff) => {
        const updated = diff.reduce(
          (acc, { key, value }) => ({ ...acc, [key]: value }),
          {},
        );
        return Result.ok({ ...t, ...updated });
      },
      (e) => Result.error(e),
    );

  readonly diff = (): DetailedDiff<T> => ({
    added: this.added,
    deleted: this.deleted,
    updated: this.updated,
  });
}
