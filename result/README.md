# @dubsar/result

A [Result](https://en.wikipedia.org/wiki/Result_type) expression in javascript.

<br>

## Features

- Simple but full-featured API
- No dependencies
- Includes a TypeScript declaration file

The library mimics [rust's](https://www.rust-lang.org/)
[Result](https://doc.rust-lang.org/std/result/enum.Result.html) with three main
goals:

- keep it simple
- pattern matching
  [syntax](https://doc.rust-lang.org/book/ch18-03-pattern-syntax.html) as close
  as possible to rust's
- expression language features

## Load

Browser:

```html
<script type="module">
  import {Result} from './path/to/result.mjs';
  ...
</script>
```

[Node.js](https://nodejs.org):

```bash
npm install @dubsar/result
```

```js
const { Result } = require("@dubsar/result");

import { Result } from "@dubsar/result";
```

## Use

Result has two builders, and no constructor:

- ok: to hold the successful result of a computation

```typescript
const success: Result<String> = Result.ok("Hello");
```

- error: to hold the error produced by an unsuccessful computation

```typescript
const failure: Result<String> = Result.error("Something webt wrong");
```

Pattern matching is achieved by the expression method match:

```typescript
interface Api {
  fetchItem: () => Result<Item>;
}

const items = (api: Api): Item[] =>
  api.fetchItem().match(
    (item) => [item],
    (error) => {
      console.error(`Something went wrong: ${error.message}`);
      return [];
    },
  );
```

An ergonomic feature is that the error handler is optional.

If a function returns a Result and wants the caller to handle errors just omit
the second argument and the error will be handled upstream.

## Licence

[The MIT Licence (Expat).](LICENCE.md)
