export const Id = <T>(t: T): T => t;
export type Ignore = Result<undefined>;

export class Result<T> {
  readonly isOk: boolean;
  readonly isError: boolean;
  private readonly value: T | Error;
  private constructor(value: T | Error, isOk: boolean) {
    this.value = value;
    this.isOk = isOk;
    this.isError = !isOk;
  }

  readonly map = <U>(map: (v: T) => U): Result<U> =>
    this.isOk ? Result.ok(map(this.get())) : Result.error(this.err());

  readonly match = <S, E = Error>(
    onOk: (v: T) => S,
    onError: (e: Error) => S | E = (e) => Result.error<T>(e) as E,
  ): S | E => this.isOk ? onOk(this.get()) : onError(this.err());

  private readonly get = (): T => this.value as T;
  private readonly err = (): Error => this.value as Error;

  readonly extract = (): T | Error => {
    console.warn("Result.extract should never be used!");
    return this.value;
  };

  static readonly ok = <T>(result: T): Result<T> => new Result(result, true);
  static readonly error = <T>(
    error: Error | string,
    cause?: Error,
  ): Result<T> => {
    const message = error instanceof Error ? error.message : error;
    const chained = new Error(message, { cause });
    return new Result<T>(chained, false);
  };

  static readonly isOk = <T>(self: Result<T>) => self.isOk;
  static readonly isError = <T>(self: Result<T>) => self.isError;

  static readonly transpose = <T>(array: Result<T>[]): Result<T[]> => {
    const result: T[] = [];
    for (const item of array) {
      if (item.isError) {
        return Result.error(item.err());
      } else {
        result.push(item.get());
      }
    }
    return Result.ok(result);
  };

  static readonly reduce = <T, U>(
    array: Result<T>[],
    reduce: (array: T[]) => U,
  ): Result<U> => {
    const transposed = Result.transpose(array);
    const result = transposed.match(
      (transposed) => {
        const reduced = reduce(transposed);
        return Result.ok<U>(reduced);
      },
      (e) => Result.error<U>(e),
    );
    return result;
  };

  static readonly try = <T>(fallible: () => Result<T>): Result<T> => {
    try {
      return fallible();
    } catch (e) {
      return Result.error(e as Error);
    }
  };

  static readonly Ignore: Ignore = Result.ok(undefined);
}
